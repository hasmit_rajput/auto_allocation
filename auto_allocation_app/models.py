from __future__ import unicode_literals

from django.db import models


EATING_HABITS = (('V', 'Veg'), ('N', 'Non-veg'))
GENDER = (('M', 'Male'), ('F', 'Female'))
RELIGION = (('H', 'Hindu'), ('M', 'Muslim'))
SHIFT = (('N', 'Night'), ('D', 'Day'))
BHCAPACITY = (('BH1', 'BH1'), ('BH2', 'BH2'),('BH3', 'BH3'))
CONDITIONCHOICES = (('C1', 'Condition 1'), ('C2', 'Condition 2'),('C3', 'Condition 3'))
SPECIALIZATIONCHOICES = (('S1', 'Specialization 1'), ('S2', 'Specialization 2'),('S3', 'Specialization 3'))


class Customer(models.Model):
    lang = models.ForeignKey('auto_allocation_app.Lang')
    eating_habit = models.CharField(max_length=1, choices=EATING_HABITS)
    gender = models.CharField(max_length=1, choices=GENDER)
    religion = models.CharField(max_length=1, choices=RELIGION)
    price = models.IntegerField()
    duration = models.IntegerField()
    shift = models.CharField(max_length=1, choices=SHIFT)
    address = models.CharField(max_length=2000)
    age = models.IntegerField()
    height = models.DecimalField(max_digits=6,decimal_places=3)
    weight = models.DecimalField(max_digits=6,decimal_places=3)
    mobility = models.BooleanField()
    conditions = models.CharField(max_length=2,choices=CONDITIONCHOICES)

    def __unicode__(self):
        return str(self.id)

class Caregiver(models.Model):
    lang = models.ForeignKey('auto_allocation_app.Lang')
    experience = models.ForeignKey('auto_allocation_app.ExperienceInHospital')
    eating_habit = models.CharField(max_length=1, choices=EATING_HABITS)
    gender = models.CharField(max_length=1, choices=GENDER)
    religion = models.CharField(max_length=1, choices=RELIGION)
    price = models.IntegerField()
    duration = models.IntegerField()
    shift = models.CharField(max_length=1, choices=SHIFT)
    address = models.CharField(max_length=2000)
    age = models.IntegerField()
    height = models.DecimalField(max_digits=6,decimal_places=3)
    weight = models.DecimalField(max_digits=6,decimal_places=3)
    specialization = models.CharField(max_length=2,choices=SPECIALIZATIONCHOICES)
    build_handling_capacity=models.CharField(max_length=3, choices=BHCAPACITY)
    basePrice=models.FloatField()
    durationPreference=models.FloatField()
    areaPreference=models.TextField()

    def __unicode__(self):
        return str(self.id)


class Lang(models.Model):
    name = models.CharField(max_length=20)

    def __unicode__(self):
        return str(self.name)


class Condition(models.Model):
    name = models.CharField(max_length=20)

    def __unicode__(self):
        return str(self.name)


class ExperienceInHospital(models.Model):
    isExperienced=models.BooleanField()
    expereince=models.DecimalField(max_digits=6,decimal_places=3) # in years

    def __unicode__(self):
        return str(self.expereince)
