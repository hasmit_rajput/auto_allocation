from __future__ import unicode_literals

from django.apps import AppConfig


class AutoAllocationAppConfig(AppConfig):
    name = 'auto_allocation_app'
