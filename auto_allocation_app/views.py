from django.shortcuts import render, HttpResponse
import json


# Create your views here.
from .models import Customer,Caregiver

def condition_filter(condition):
    condition_construct = {
        'C1' : 'S1',
        'C2' : 'S2',
        'C3' : 'S3'
    }

    if condition_construct[condition]:
        return condition_construct[condition],True
    else:
        return None,False


def build_filter(age):
    age_numeric = int(age)
    if age_numeric < 60:
       return 'BH1',True
    elif 60 < age_numeric < 80:
        return 'BH2', True
    elif 80 < age_numeric:
        return 'BH3',True


def apply_filters_on_cgs(request):
    customer=Customer.objects.get(id=request.GET.get('customer_id'))
    specialization, spec_status=condition_filter(customer.conditions)
    build, status=build_filter(customer.age)

    if spec_status==True:
        cgs=Caregiver.objects.filter(specialization=specialization,build_handling_capacity=build)

    if cgs.count()>0:
        cgList=[]
        cgListFiltered1=[]
        cgListFiltered2=[]
        cgListFiltered3=[]
        cgListFiltered4=[]
        for cg in cgs:
            if cg.eating_habit==customer.eating_habit:
                cgList.append(cg)
        if len(cgList)>0:
            if customer.gender=="M":
                for i in cgList:
                    if i.gender=='M':
                        cgListFiltered1.append(i)
            else:
                cgListFiltered1=cgList
        if len(cgListFiltered1)>0:
            for i in cgListFiltered1:
                if customer.religion==cg.religion:
                    cgListFiltered2.append(cg)
        if len(cgListFiltered2)>0:
            for i in cgListFiltered2:
                if customer.lang==cg.lang:
                    cgListFiltered3.append(cg)
        if len(cgListFiltered3)>0:
            print cgListFiltered3
            for cg in cgListFiltered3:
                cg_dict={
                        "cg_id":cg.id,
                }
                cgListFiltered4.append(cg_dict)
                
            return HttpResponse(json.dumps({'cgList': cgListFiltered4, 'status': 200}), content_type="application/json")
    else:
        return HttpResponse(json.dumps({'validation': 'Cg not found', 'status': 404}), content_type="application/json")
