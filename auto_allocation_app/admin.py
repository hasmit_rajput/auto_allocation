from django.contrib import admin
from models import Customer, Caregiver, Lang, Condition, ExperienceInHospital
# Register your models here.

admin.site.register(Customer)
admin.site.register(Caregiver)
admin.site.register(Lang)
admin.site.register(Condition)
admin.site.register(ExperienceInHospital)