# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auto_allocation_app', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='caregiver',
            name='specialization',
            field=models.CharField(max_length=2, choices=[('S1', 'Specialization 1'), ('S2', 'Specialization 2'), ('S3', 'Specialization 3')]),
        ),
        migrations.AlterField(
            model_name='customer',
            name='conditions',
            field=models.CharField(max_length=2, choices=[('C1', 'Condition 1'), ('C2', 'Condition 2'), ('C3', 'Condition 3')]),
        ),
        migrations.DeleteModel(
            name='Specialization',
        ),
    ]
