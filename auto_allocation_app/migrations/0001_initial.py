# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Caregiver',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('eating_habit', models.CharField(max_length=1, choices=[('V', 'Veg'), ('N', 'Non-veg')])),
                ('gender', models.CharField(max_length=1, choices=[('M', 'Male'), ('F', 'Female')])),
                ('religion', models.CharField(max_length=1, choices=[('H', 'Hindu'), ('M', 'Muslim')])),
                ('price', models.IntegerField()),
                ('duration', models.IntegerField()),
                ('shift', models.CharField(max_length=1, choices=[('N', 'Night'), ('D', 'Day')])),
                ('address', models.CharField(max_length=2000)),
                ('age', models.IntegerField()),
                ('height', models.DecimalField(max_digits=6, decimal_places=3)),
                ('weight', models.DecimalField(max_digits=6, decimal_places=3)),
                ('build_handling_capacity', models.CharField(max_length=3, choices=[('BH1', 'BH1'), ('BH2', 'BH2'), ('BH3', 'BH3')])),
                ('basePrice', models.FloatField()),
                ('durationPreference', models.FloatField()),
                ('areaPreference', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Condition',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('eating_habit', models.CharField(max_length=1, choices=[('V', 'Veg'), ('N', 'Non-veg')])),
                ('gender', models.CharField(max_length=1, choices=[('M', 'Male'), ('F', 'Female')])),
                ('religion', models.CharField(max_length=1, choices=[('H', 'Hindu'), ('M', 'Muslim')])),
                ('price', models.IntegerField()),
                ('duration', models.IntegerField()),
                ('shift', models.CharField(max_length=1, choices=[('N', 'Night'), ('D', 'Day')])),
                ('address', models.CharField(max_length=2000)),
                ('age', models.IntegerField()),
                ('height', models.DecimalField(max_digits=6, decimal_places=3)),
                ('weight', models.DecimalField(max_digits=6, decimal_places=3)),
                ('mobility', models.BooleanField()),
                ('conditions', models.ForeignKey(to='auto_allocation_app.Condition')),
            ],
        ),
        migrations.CreateModel(
            name='ExperienceInHospital',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('isExperienced', models.BooleanField()),
                ('expereince', models.DecimalField(max_digits=6, decimal_places=3)),
            ],
        ),
        migrations.CreateModel(
            name='Lang',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Specialization',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=20)),
            ],
        ),
        migrations.AddField(
            model_name='customer',
            name='lang',
            field=models.ForeignKey(to='auto_allocation_app.Lang'),
        ),
        migrations.AddField(
            model_name='caregiver',
            name='experience',
            field=models.ForeignKey(to='auto_allocation_app.ExperienceInHospital'),
        ),
        migrations.AddField(
            model_name='caregiver',
            name='lang',
            field=models.ForeignKey(to='auto_allocation_app.Lang'),
        ),
        migrations.AddField(
            model_name='caregiver',
            name='specialization',
            field=models.ForeignKey(to='auto_allocation_app.Specialization'),
        ),
    ]
